<?php

/**
 * @file
 * Install, update and uninstall for UW Scholar.
 */

use Drupal\user\Entity\Role;
use Drupal\uw_cfg_common\UwPermissions\UwPermissions;
use Drupal\uw_cfg_common\UwRoles\UwRoles;
use Symfony\Component\Yaml\Yaml;

/**
 * Implements hook_install().
 */
function uw_cfg_scholar_install() {

  $uw_rid = 'uw_role_scholar';
  $uw_role_label = 'Scholar';

  if (!Role::load($uw_rid)) {
    $role = Role::create(
      [
        'id' => $uw_rid,
        'label' => $uw_role_label,
      ]
    );

    $role->save();
  }

  // Get the info about the role.
  $uw_role = UwRoles::getUwRole($uw_rid);

  // Set the permissions for the role.
  UwRoles::setUwPermissions($uw_role);

  $uw_roles = UwPermissions::getRoles('Scholar');

  // Build the access permissions array.
  $module_handler = \Drupal::service('module_handler');
  $module_path = $module_handler->getModule('uw_cfg_scholar')->getPath();
  $permissions_to_process = Yaml::parseFile($module_path . '/includes/scholar_access_content_permissions.yml');
  UwPermissions::grantRevoke($permissions_to_process, 'grant', $uw_roles);
}
