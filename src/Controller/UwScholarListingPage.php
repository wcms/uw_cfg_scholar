<?php

namespace Drupal\uw_cfg_scholar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for scholar listing page.
 */
class UwScholarListingPage extends ControllerBase {

  /**
   * The variable for current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Entity type interface manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UwScholarListingPage constructor.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    CurrentPathStack $currentPath,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->currentPath = $currentPath;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates this form class.
    return new static(
      $container->get('path.current'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function listingPage($username) {

    // Get the current path.
    $current_path = $this->currentPath->getPath();

    // Break the path into parts on the slash.
    $path_parts = explode('/', $current_path);

    // Get the listing page type with is the 4th element
    // in the array.
    $listing_page = $path_parts[3];

    // Get the view based on the listing image.
    switch ($listing_page) {

      // Get the blog view.
      case 'blog':
        $view = $this->entityTypeManager
          ->getStorage('view')
          ->load('uw_view_blogs')
          ->getExecutable();
        $view->setDisplay('blog_page');
        break;

      // Get the event view.
      case 'events':
        $view = $this->entityTypeManager
          ->getStorage('view')
          ->load('uw_view_events')
          ->getExecutable();
        $view->setDisplay('event_page');
        break;

      // Get the catalogs view.
      case 'catalogs':
        $view = $this->entityTypeManager
          ->getStorage('view')
          ->load('uw_view_catalogs')
          ->getExecutable();
        $view->setDisplay('catalogs_page');

        $view->addHandler(
          $view->current_display,
          'filter',
          'node_field_data',
          'uid',
          [
            'value' => [
              'value' => _uw_get_scholar_info('uid'),
            ],
            'relationship' => 'reverse__node__field_uw_catalog_catalog',
            'group_type' => 'group',
            'group' => 1,
          ]
        );
        break;

      // Get the news view.
      case 'news':
        $view = $this->entityTypeManager
          ->getStorage('view')
          ->load('uw_view_news_items')
          ->getExecutable();
        $view->setDisplay('news_page');
        break;
    }

    // Execute the view.
    $view->execute();

    // Set the render array.
    $build = [
      '#theme' => 'uw_scholar_listing_page',
      '#view' => $view->render(),
    ];

    return $build;
  }

}
