<?php

namespace Drupal\uw_cfg_scholar\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure uw scholar menu settings.
 */
class UwScholarMenuForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uw_scholar_menu.settings';

  /**
   * Entity type interface manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    AccountProxyInterface $currentUser
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_scholar_menu_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get the config.
    $config = $this->config(static::SETTINGS);

    // Get the menus from the config.
    $menus = $config->get('menus');

    // Get the username of current scholar user.
    $username = $this->currentUser->getAccountName();

    // If there is no config, get default values.
    // If there is config, load by the username.
    if ($menus == NULL || !isset($menus[$username])) {
      $menus = _uw_get_scholar_user_menu_links();
    }
    else {

      // Get the current menus of the scholar user.
      $menus = $menus[$username];

      // Get all the menus for the scholar user, just in
      // case there are some that are added.
      $all_menus = _uw_get_scholar_user_menu_links();

      // Get the differences between current and possible
      // added menus.
      $diffs = array_diff(
        array_column($all_menus, 'id'),
        array_column($menus, 'id')
      );

      // If there are differences add them to the menus.
      if (count($diffs) > 0) {
        foreach (array_keys($diffs) as $diff) {
          $menus[] = $all_menus[$diff];
        }
      }
    }

    // The field set for the menus.
    $form['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Menu links'),
      '#prefix' => '<div id="uw-scholar-menus-wrapper">',
      '#suffix' => '</div>',
    ];

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // Build the table.
    $form['items_fieldset']['items'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Menu link'),
        $this->t('Enabled'),
        // @todo Make this work properly with the remove button.
        // phpcs:disable
        // '',
        // phpcs:enable
        $this->t('Weight'),
      ],
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
    ];

    // Step through each of the menu items and set the
    // rows in the table.
    foreach ($menus as $menu) {

      // Reset the settings array.
      $settings = [];

      // Set the table to be draggable.
      $settings['#attributes']['class'][] = 'draggable';

      // Set the weight.
      $settings['#weight'] = $menu['weight'];

      // Set the id of the menu link.
      $settings['menu_name']['id'] = [
        '#type' => 'hidden',
        '#default_value' => $menu['id'],
      ];

      // Set the menu link name.
      $settings['menu_name']['menu_name'] = [
        '#type' => 'hidden',
        '#default_value' => $menu['menu_name'],
      ];

      // Set the markup for the menu name.
      $settings['menu_name']['name'] = [
        '#type' => 'markup',
        '#markup' => $menu['menu_name'],
      ];

      // Set the enabled for the menu link.
      $settings['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $menu['enabled'],
      ];

      // The weight element.
      $settings['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $menu['weight'] ?? 0,
        '#attributes' => ['class' => [$group_class]],
        '#delta' => count($menus),
      ];

      // Add the settings to the items array, which is full row
      // in the table.
      $form['items_fieldset']['items'][] = $settings;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the username of the current user.
    $username = $this->currentUser->getAccountName();

    // Get the menus from the config.
    $menus = $this->config(static::SETTINGS)->get('menus');

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the menu items from the values.
    $items = $values['items'];

    // Sort the items by the weight.
    usort($items, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });

    // Step through each item and set the new items with
    // the correct indexes in array.
    foreach ($items as $item) {
      $new_items[] = [
        'id' => $item['menu_name']['id'],
        'menu_name' => $item['menu_name']['menu_name'],
        'enabled' => $item['enabled'],
        'weight' => $item['weight'],
      ];
    }

    // Set the menus for the username.
    $menus[$username] = $new_items;

    // Save the config.
    $this->config(static::SETTINGS)
      ->set('menus', $menus)
      ->save();
  }

}
